//
//  SquareOscillator.hpp
//  JuceBasicAudio
//
//  Created by Conor Bailey on 11/11/2016.
//
//

#ifndef H_SQUAREOSCILLATOR
#define H_SQUAREOSCILLATOR

#include <stdio.h>
#include "Oscillator.hpp"

class SquareOscillator : public Oscillator
{
    
public:

    SquareOscillator();
    ~SquareOscillator();
    
    float renderWaveShape (const float currentPhase) override;

    
private:
    
    
};


#endif //H_SQUAREOSCILLATOR