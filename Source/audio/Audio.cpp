/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.setMidiInputEnabled("Impulse  Impulse", true);
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    oscPtr = &sinOscillator;
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    DBG("midi");
    oscPtr.get()->setFrequency(message.getMidiNoteInHertz(message.getNoteNumber()));
    oscPtr.get()->setAmplitude(message.getVelocity() / 127.f);
    
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    
    while(numSamples--)
    {
        
        beep();
        
        float value = oscPtr.get()->nextSample();
        *outL = value;
        *outR = value;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    oscPtr.get()->setSampleRate(device->getCurrentSampleRate());
}

void Audio::audioDeviceStopped()
{

}

void Audio::beep()
{
//    oscillator.setFrequency(440.f);
//    oscillator.setAmp(1.f);
    
    
}

void Audio::setWaveform(int newWaveform)
{
    if (newWaveform == 1)
    {
        oscPtr = &sinOscillator;
        
    }

    
    if (newWaveform == 2)
    {
        oscPtr = &squareOscillator;
        
    }

}
