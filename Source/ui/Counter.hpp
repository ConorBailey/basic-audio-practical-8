//
//  Counter.hpp
//  JuceBasicAudio
//
//  Created by Conor Bailey on 07/11/2016.
//
//

#ifndef Counter_hpp
#define Counter_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class Counter : public Thread
{
public:
    Counter();
    ~Counter();
    
    void run() override;
    void start();
    
    /**class for counter listeners to inherit*/
    class Listener
    {
    public:
        //deconstructor
        virtual ~Listener()
        {
            
        }
        
        //called when next timer reached next interval
        virtual void counterChanged (const unsigned int counterValue) = 0;
    };
    void setListener (Listener* newListener);

private:
    
    int timeOffset;
    Listener* listener;
};

#endif /* Counter_hpp */
